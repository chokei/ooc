package main

import (
	"ooc/core/gameplay/task_module"
	"time"
)

func main() {
	eventManager := task_module.NewGameEventManager()
	task1 := &task_module.Task{TaskID: "任务1", Status: "未完成", EventType: "合拍", EventParameter: "1号NPC", RequiredCount: 2}
	task2 := &task_module.Task{TaskID: "任务2", Status: "未完成", EventType: "放生", EventParameter: "锦鲤", RequiredCount: 1}
	eventManager.Subscribe(task1)
	eventManager.Subscribe(task2)
	eventManager.TriggerEvent(task_module.GameEvent{EventType: "合拍", EventParameter: "1号NPC"})
	eventManager.TriggerEvent(task_module.GameEvent{EventType: "放生", EventParameter: "锦鲤"})

	time.Sleep(time.Second * 1)
}
