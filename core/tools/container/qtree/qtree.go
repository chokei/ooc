package qtree

import (
	"ooc/core/tools/constraints"
	"ooc/core/tools/geometry"
)

var (
	Capacity = 8
	MaxDepth = 6
)

type (
	typ interface {
		constraints.Signed | constraints.Float
	}

	Tree[T typ] struct {
		maxCapacity, maxDepth int
		root                  *Node[T]
	}
)

func NewTree[T typ](a *AABB2D[T]) *Tree[T] {
	return &Tree[T]{
		maxCapacity: Capacity,
		maxDepth:    MaxDepth,
		root: &Node[T]{
			boundary: a,
			depth:    0,
			children: [4]*Node[T]{},
		},
	}
}

func (slf *Tree[T]) Insert(p *geometry.Point2D[T]) bool {
	return slf.root.Insert(p)
}

func (slf *Tree[T]) Remove(p *geometry.Point2D[T]) bool {
	return slf.root.Remove(p)
}

func (slf *Tree[T]) Update(p, np *geometry.Point2D[T]) bool {
	return slf.root.Update(p, np)
}

func (slf *Tree[T]) Search(a *AABB2D[T]) []*geometry.Point2D[T] {
	return slf.root.Search(a)
}

func (slf *Tree[T]) KNearest(a *AABB2D[T], i int, fn filter[T]) []*geometry.Point2D[T] {
	v := make(map[*Node[T]]bool)
	return slf.root.KNearest(a, i, v, fn)
}

type AABB2D[T typ] struct {
	Center *geometry.Point2D[T]
	Half   *geometry.Point2D[T]
}

//type Point struct {
//	x    float64
//	y    float64
//	data interface{}
//}

type Node[T typ] struct {
	boundary *AABB2D[T]
	depth    int
	points   []*geometry.Point2D[T]
	parent   *Node[T]
	children [4]*Node[T]
}

type filter[T typ] func(d *geometry.Point2D[T]) bool

// NewAABB creates an axis aligned bounding box. It takes the Center and Half
// point.
func NewAABB[T typ](center, half *geometry.Point2D[T]) *AABB2D[T] {
	return &AABB2D[T]{center, half}
}

//// NewPoint generates a new *Point struct.
//func NewPoint(x, y float64, data interface{}) *Point {
//	return &Point{x, y, data}
//}

// ContainsPoint checks whether the point provided resides within the axis
// aligned bounding box.
func (a *AABB2D[T]) Contains(p *geometry.Point2D[T]) bool {
	if p.GetX() < a.Center.GetX()-a.Half.GetX() {
		return false
	}
	if p.GetY() < a.Center.GetY()-a.Half.GetY() {
		return false
	}
	if p.GetX() > a.Center.GetX()+a.Half.GetX() {
		return false
	}
	if p.GetY() > a.Center.GetY()+a.Half.GetY() {
		return false
	}

	return true
}

// Intersect checks whether two axis aligned bounding boxes overlap.
func (a *AABB2D[T]) Intersect(b *AABB2D[T]) bool {
	if b.Center.GetX()+b.Half.GetX() < a.Center.GetX()-a.Half.GetX() {
		return false
	}
	if b.Center.GetY()+b.Half.GetY() < a.Center.GetY()-a.Half.GetY() {
		return false
	}
	if b.Center.GetX()-b.Half.GetX() > a.Center.GetX()+a.Half.GetX() {
		return false
	}
	if b.Center.GetY()-b.Half.GetY() > a.Center.GetY()+a.Half.GetY() {
		return false
	}

	return true
}

//// Coordinates return the x and y coordinates of a point.
//func (p *Point) Coordinates() (float64, float64) {
//	return p.x, p.y
//}
//
//// Data returns the data stored within a point.
//func (p *Point) Data() interface{} {
//	return p.data
//}

func (slf *Node[T]) divide() {
	if slf.children[0] != nil {
		return
	}

	bb := &AABB2D[T]{
		Center: geometry.NewPoint2D(slf.boundary.Center.GetX()-slf.boundary.Half.GetX()/2, slf.boundary.Center.GetY()+slf.boundary.Half.GetY()/2),
		Half:   geometry.NewPoint2D(slf.boundary.Half.GetX()/2, slf.boundary.Half.GetY()/2),
	}
	slf.children[0] = &Node[T]{
		boundary: bb,
		depth:    slf.depth + 1,
		parent:   slf,
	}

	bb = &AABB2D[T]{
		Center: geometry.NewPoint2D(slf.boundary.Center.GetX()+slf.boundary.Half.GetX()/2, slf.boundary.Center.GetY()+slf.boundary.Half.GetY()/2),
		Half:   geometry.NewPoint2D(slf.boundary.Half.GetX()/2, slf.boundary.Half.GetY()/2),
	}
	slf.children[1] = &Node[T]{
		boundary: bb,
		depth:    slf.depth + 1,
		parent:   slf,
	}

	bb = &AABB2D[T]{
		Center: geometry.NewPoint2D(slf.boundary.Center.GetX()-slf.boundary.Half.GetX()/2, slf.boundary.Center.GetY()-slf.boundary.Half.GetY()/2),
		Half:   geometry.NewPoint2D(slf.boundary.Half.GetX()/2, slf.boundary.Half.GetY()/2),
	}
	slf.children[2] = &Node[T]{
		boundary: bb,
		depth:    slf.depth + 1,
		parent:   slf,
	}

	bb = &AABB2D[T]{
		Center: geometry.NewPoint2D(slf.boundary.Center.GetX()+slf.boundary.Half.GetX()/2, slf.boundary.Center.GetY()-slf.boundary.Half.GetY()/2),
		Half:   geometry.NewPoint2D(slf.boundary.Half.GetX()/2, slf.boundary.Half.GetY()/2),
	}
	slf.children[3] = &Node[T]{
		boundary: bb,
		depth:    slf.depth + 1,
		parent:   slf,
	}

	for _, p := range slf.points {
		for _, node := range slf.children {
			if node.Insert(p) {
				break
			}
		}
	}

	slf.points = nil
}

func (slf *Node[T]) nearest(a *AABB2D[T], i int, v map[*Node[T]]bool, fn filter[T]) []*geometry.Point2D[T] {
	var results []*geometry.Point2D[T]

	if _, ok := v[slf]; ok {
		return results
	} else {
		v[slf] = true
	}

	if !slf.boundary.Intersect(a) {
		return results
	}

	for _, p := range slf.points {
		if a.Contains(p) {
			results = append(results, p)
		}

		if len(results) >= i {
			return results[:i]
		}
	}

	if slf.children[0] != nil {
		for _, node := range slf.children {
			results = append(results, node.nearest(a, i, v, fn)...)

			if len(results) >= i {
				return results[:i]
			}
		}
		if len(results) >= i {
			results = results[:i]
		}
	}

	if slf.parent == nil {
		return results
	}

	results = append(results, slf.parent.nearest(a, i, v, fn)...)

	if len(results) >= i {
		results = results[:i]
	}
	return results
}

// Insert will attempt to insert the point into the Node. It will
// recursively search until it finds the leaf node. If the leaf node
// is at capacity then it will try split the node. If the tree is at
// max depth then point will be stored in the leaf.
func (slf *Node[T]) Insert(p *geometry.Point2D[T]) bool {
	if !slf.boundary.Contains(p) {
		return false
	}

	if slf.children[0] == nil {
		if len(slf.points) < Capacity {
			slf.points = append(slf.points, p)
			return true
		}

		if slf.depth < MaxDepth {
			slf.divide()
		} else {
			slf.points = append(slf.points, p)
			return true
		}
	}

	for _, node := range slf.children {
		if node.Insert(p) {
			return true
		}
	}

	return false
}

// Remove attempt to remove a point from the Node. It will recurse until
// the leaf node is found and then try to remove the point.
func (slf *Node[T]) Remove(p *geometry.Point2D[T]) bool {
	if !slf.boundary.Contains(p) {
		return false
	}

	if slf.children[0] == nil {
		for i, ep := range slf.points {
			if ep != p {
				continue
			}

			// remove point
			if last := len(slf.points) - 1; i == last {
				slf.points = slf.points[:last]
			} else {
				slf.points[i] = slf.points[last]
				slf.points = slf.points[:last]
			}
			return true
		}

		return false
	}

	for _, node := range slf.children {
		if node.Remove(p) {
			return true
		}
	}

	return false
}

// ReverseInsert is used in conjunction with Update to try reverse insert a point.
func (slf *Node[T]) ReverseInsert(p *geometry.Point2D[T]) bool {
	// Try insert down the tree
	if slf.Insert(p) {
		return true
	}

	// hit root node
	if slf.parent == nil {
		return false
	}

	// try reverse insert parent
	return slf.parent.ReverseInsert(p)
}

// Update will update the location of a point within the tree. It is
// optimised to attempt reinsertion within the same node and recurse
// back up the tree until it finds a suitable node.
func (slf *Node[T]) Update(p, np *geometry.Point2D[T]) bool {
	if !slf.boundary.Contains(p) {
		return false
	}

	// At the leaf
	if slf.children[0] == nil {
		for i, ep := range slf.points {
			if ep != p {
				continue
			}

			// set new coords
			p.SetX(np.GetX())
			p.SetY(np.GetY())

			// now do we move?
			if slf.boundary.Contains(np) {
				return true
			}

			// remove from current node
			if last := len(slf.points) - 1; i == last {
				slf.points = slf.points[:last]
			} else {
				slf.points[i] = slf.points[last]
				slf.points = slf.points[:last]
			}

			// well shit now...reinsert
			return slf.ReverseInsert(p)
		}
		return false
	}

	for _, node := range slf.children {
		if node.Update(p, np) {
			return true
		}
	}

	return false
}

// Search will return all the points within the given axis aligned bounding
// box. It recursively searches downward through the tree.
func (slf *Node[T]) Search(a *AABB2D[T]) []*geometry.Point2D[T] {
	var results []*geometry.Point2D[T]

	if !slf.boundary.Intersect(a) {
		return results
	}

	for _, p := range slf.points {
		if a.Contains(p) {
			results = append(results, p)
		}
	}

	if slf.children[0] == nil {
		return results
	}

	for _, node := range slf.children {
		results = append(results, node.Search(a)...)
	}

	return results
}

// KNearest returns the k nearest points within the Node that fall within
// the bounds of the axis aligned bounding box. A filter function can be used
// which is evaluated against each point. The search begins at the leaf and
// recurse towards the parent until k nearest have been found or root node is
// hit.
func (slf *Node[T]) KNearest(a *AABB2D[T], i int, v map[*Node[T]]bool, fn filter[T]) []*geometry.Point2D[T] {
	var results []*geometry.Point2D[T]

	if !slf.boundary.Intersect(a) {
		return results
	}

	// hit the leaf
	if slf.children[0] == nil {
		results = append(results, slf.nearest(a, i, v, fn)...)

		if len(results) >= i {
			results = results[:i]
		}

		return results
	}

	for _, node := range slf.children {
		results = append(results, node.KNearest(a, i, v, fn)...)

		if len(results) >= i {
			return results[:i]
		}
	}

	if len(results) >= i {
		results = results[:i]
	}

	return results
}
