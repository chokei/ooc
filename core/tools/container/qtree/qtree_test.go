package qtree_test

import (
	"fmt"
	"ooc/core/tools/container/qtree"
	"ooc/core/tools/geometry"
	"testing"
)

func TestName(t *testing.T) {
	centerPoint := geometry.NewPoint2D(0.0, 0.0)
	halfPoint := geometry.NewPoint2D(32.0, 32.0)
	boundingBox := &qtree.AABB2D[float64]{
		Center: centerPoint,
		Half:   halfPoint,
	}

	tree := qtree.NewTree(boundingBox)

	for i := -32; i < 32; i++ {
		if !tree.Insert(geometry.NewPoint2D(float64(i), float64(i))) {
			t.Fatal("failed")
		}
	}

	points := tree.KNearest(boundingBox, 64, nil)
	fmt.Println(points)
}
