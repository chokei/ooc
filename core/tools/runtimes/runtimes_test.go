package runtimes_test

import (
	"testing"

	"ooc/core/tools/log"
	"ooc/core/tools/runtimes"
)

func TestCurrentFuncName(t *testing.T) {
	fnName := runtimes.CurrentFuncName()
	log.Info(fnName)
}

func TestGetWorkingDir(t *testing.T) {
	dir := runtimes.GetWorkingDir()
	log.Info(dir)
}
