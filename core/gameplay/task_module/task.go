package task_module

import (
	"fmt"
	"ooc/core/tools/inbox"
)

type GameEvent struct {
	EventType      string
	EventParameter string
}

type GameEventHandler interface {
	HandleEvent(event GameEvent)
}

type GameEventManager struct {
	inbox       *inbox.Inbox[GameEvent]
	subscribers []GameEventHandler
}

func NewGameEventManager() *GameEventManager {
	mgr := &GameEventManager{
		inbox: inbox.NewInbox[int, GameEvent](100),
	}
	processor := &GameEventProcessor{mgr}
	mgr.inbox.Start(processor)
	return mgr
}

func (gem *GameEventManager) Subscribe(handler GameEventHandler) {
	gem.subscribers = append(gem.subscribers, handler)
}

func (gem *GameEventManager) TriggerEvent(event GameEvent) {
	gem.inbox.Push(event)
}

type Task struct {
	TaskID         string
	Status         string
	EventType      string
	EventParameter string
	RequiredCount  int
	CurrentCount   int
}

func (t *Task) HandleEvent(event GameEvent) {
	if event.EventType == t.EventType && event.EventParameter == t.EventParameter {
		t.CurrentCount++
		fmt.Println(t.TaskID, "任务进度", t.CurrentCount, "/", t.RequiredCount)
		if t.CurrentCount >= t.RequiredCount {
			t.Status = "已完成"
			fmt.Println(t.TaskID, "任务完成")
		}
	}
}

type GameEventProcessor struct {
	eventManager *GameEventManager
}

func (gep *GameEventProcessor) Invoke(events []GameEvent) {
	for _, event := range events {
		for _, handler := range gep.eventManager.subscribers {
			handler.HandleEvent(event)
		}
	}
}
