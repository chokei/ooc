package network

import "ooc/core/tools/log"

type RunMode = log.RunMode

const (
	RunModeDev  RunMode = log.RunModeDev
	RunModeTest RunMode = log.RunModeTest
	RunModeProd RunMode = log.RunModeProd
)
